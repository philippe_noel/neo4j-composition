# Docker composition for neo4j

## Requirements
- docker-compose
- direnv
- docker

## How to use
- The makefile is the entrypoint for all actions. Use `make` or `make help` to get the full list of command.
- All variables are set in the .env.sample file. This file is expand to an .env (used by docker) and .envrc (used by direnv) files. If you edit the .env.sample file, remove .env and .envrc and use `make `
- To run, just use:
``` bash
make up 
make open_neo4j
```
in your browser, use no_authentication (if you didn't edit the config file) and use url bolt://0.0.0.0:7687

## Certificates
You should generate certificates to enable https. Put neo4j.key and neo4j.cert in neo4j/certificates/ folder

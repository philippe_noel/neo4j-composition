#!/bin/bash

## Bash script to expand the file $1 with variables in the $2 file
## $1 : input file. File with variables to expand
## $2 : output file

INPUT_FILE="$1"
OUTPUT_FILE="$2"

eval "cat << EOF
$(<"${INPUT_FILE}")
EOF
" > "${OUTPUT_FILE}"
